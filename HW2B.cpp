#include <iostream>
#include <chrono>
using namespace std;

int main() {
	int n, i, j, i1, j1;

	cout << "Number of rows and columns in matrix: ";
	cin >> n;

	int* A = (int*) malloc(n * n * sizeof(int));
	int* B = (int*) malloc(n * n * sizeof(int));
	for (i = 0; i < n; i++){
		for (j = 0; j < n; j++){
			*(A + i * n + j) = i;
		}
	}

	chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

	int bsize = 16;

	for (i = 0; i < n; i += bsize){
		for (j = 0; j < n; j += bsize){
			for (i1 = i; i1 < std::min(i+bsize-1,n); i1++){
				for (j1 = j; j1 < std::min(j+bsize-1,n); j1++){
					*(B + i1 * n + j1) = *(A + j1 * n + i1);
				}
			}
		}
	}

	chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
	cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << " microseconds" <<std::endl;

	delete [] A;
	delete [] B;
	return 0;
}
